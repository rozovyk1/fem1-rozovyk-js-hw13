/*
Задание

Реализовать возможность смены цветовой темы сайта пользователем.

Технические требования:

Взять любое готовое домашнее задание по HTML/CSS.
Добавить на макете кнопку "Сменить тему".
При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
Выбранная тема должна сохраняться и после перезагрузки страницы.

*/

document.getElementById('change-theme-btn').addEventListener('click', function () {
    let darkThemeEnabled = document.body.classList.toggle('dark-theme');
    localStorage.setItem('dark-theme-enable', darkThemeEnabled);
});

if (localStorage.getItem('dark-theme-enable') == 'true') {
    document.body.classList.add('dark-theme');
}
